package br.com.agendee.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.agendee.entity.Usuario;
import br.com.agendee.service.UsuarioService;

@RestController
@RequestMapping("/usuarios")
public class UsuarioController {
	
	@Autowired
	UsuarioService usuarioService;
	
	@GetMapping
	public List<Usuario> listarUsuarios() {
		return usuarioService.listarUsuarios();
	}
	
	@GetMapping("/{seqUsuario}")
	public Usuario buscarUsuario(@PathVariable("seqUsuario") Integer seqUsuario) {
		return usuarioService.buscarUsuario(seqUsuario);
	}
	
	@PostMapping
	public void criarUsuario() {
		
	}

}
