package br.com.agendee;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AgendeeBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(AgendeeBackendApplication.class, args);
	}

}
