package br.com.agendee.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.agendee.entity.Usuario;
import br.com.agendee.repository.UsuarioRepository;

@Service
public class UsuarioService {
	
	@Autowired
	UsuarioRepository usuarioRepository;

	public List<Usuario> listarUsuarios() {
		return usuarioRepository.findAll();
	}

	public Usuario buscarUsuario(Integer seqUsuario) {
		return usuarioRepository.findBySeqUsuario(seqUsuario);
	}

}
